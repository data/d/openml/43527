# OpenML dataset: Malware-Analysis-Datasets-PE-Section-Headers

https://www.openml.org/d/43527

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Introduction
This dataset is part of my PhD research on malware detection and classification using Deep Learning. It contains static analysis data (PE Section Headers of the .text, .code and CODE sections) extracted from the 'pe_sections' elements of Cuckoo Sandbox reports. PE malware examples were downloaded from virusshare.com. PE goodware examples were downloaded from portableapps.com and from Windows 7 x86 directories.
Features
Column name: hash
Description: MD5 hash of the example
Content: 32 bytes string
Column name: sizeofdata
Description: The size of the section on disk
Content: Integer
Column name: virtualaddress
Description: Memory address of the first byte of the section relative to the image base
Content: Integer
Column name: entropy
Description: Calculated entropy of the section
Content: Float
Column name: virtualsize
Description: The size of the section when loaded into memory
Content: Integer
Column name: malware
Description: Class
Content: 0 (Goodware) or 1 (Malware)
Acknowledgements
Thank you Cuckoo Sandbox for developing such an amazing dynamic analysis environment!
Thank you VirusShare! Because sharing is caring!
Citations
Please refer to http://dx.doi.org/10.21227/2czh-es14

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43527) of an [OpenML dataset](https://www.openml.org/d/43527). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43527/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43527/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43527/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

